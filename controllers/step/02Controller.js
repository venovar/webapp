"use strict";
application
	.controller('Step02Controller', function (
		$rootScope,
		$state,
		$http,
		config, 
		$scope)
	{
		$rootScope.navigation = "step02";
		if(!$rootScope.data) {
			$state.go('step01');
		}
		$scope.submit = function () {
			$http({
				url 				: config.service_url + '/fakes',
				method  			: 'POST',
				headers : {
					'Authorization' : '​Bearer​ ​NOVERDEFAKETOKEN'
				},
				data 				: $rootScope.data
			}).then(function(response) {
				if(response.status == 200) {
					if(response.data.status == 'denied') {
						$state.go('unsuccess');
					} else {
						$state.go('step03', { responseData : response.data });
					}
				} else {
					alert("Não foi possíve completar sua solicitação. Contacte-nos!");
					delete $rootScope.data;
					$state.go('step01');
				}
			});
		}
	})
;