"use strict";
application
	.controller('UnSuccessController', function (
		$rootScope,
		$state,
		$http,
		config, 
		$scope)
	{
		$rootScope.navigation = "unsuccess";
		if(!$rootScope.data) {
			$state.go('step01');
		}
	})
;