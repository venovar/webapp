"use strict";
application
	.controller('Step01Controller', function (
		$rootScope,
		$state,
		$http, 
		$scope,
		config)
	{
		$rootScope.navigation = "step01";
		if(!$rootScope.data)
			$rootScope.data = {};
		$scope.submit = function () {
			$state.go('step02');
		}
	})
;