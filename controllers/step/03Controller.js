"use strict";
application
	.controller('Step03Controller', function (
		$rootScope,
		$state,
		$http,
		config, 
		$stateParams,
		$scope)
	{
		$rootScope.navigation = "step03";
		if(!$rootScope.data) {
			$state.go('step01');
		}
		$scope.responseData = $stateParams.responseData;
	})
;