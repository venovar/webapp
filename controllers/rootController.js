"use strict";
application
	.controller('RootController', function (
		$http,
		$state,
		$window,
		$timeout,
		$rootScope,
		$location,
		config) 
	{
    	moment.locale('pt-BR');
    	
    	$rootScope.$on('loading', function(event, flag) {
			if(flag) 	
				angular.element('div[id="loader"]').show();
			else 		
				angular.element('div[id="loader"]').hide();
			$rootScope.loading = flag;
		});
		
		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			$rootScope.$offAll('error');
		});
		
		$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
			$rootScope.$on('error', error);
		});
		
		$rootScope.$on('request', function() {
			$rootScope.$broadcast('loading', true);
		});
		
		$rootScope.$on('response', function() {
			$rootScope.$broadcast('loading', false);
		});
		
		$rootScope.backPageTo = function (to, params) {
			angular.element('.container > div').removeClass('rtl');
			angular.element('.container > div').addClass('ltr');
			$timeout(function() { $state.go(to, params); }, 1);
		};
		
		function error (event, data, status) {
			console.error("Erro!");
		};
	})
	.run(function($rootScope, $state, $window){
		// Remove todos eventListners
	    $rootScope.$off = function (name, listener) {
	        var namedListeners = this.$$listeners[name];
	        if(namedListeners) {
	            for (var i = 0; i < namedListeners.length; i++) {
	                if(namedListeners[i] === listener) {
	                    return namedListeners.splice(i, 1);
	                }
	            }
	        }
	    }
	    // Remove um eventListner
	    $rootScope.$offAll = function (name) {
	        delete this.$$listeners[name];
	    }
	})
;