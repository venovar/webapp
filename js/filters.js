"use strict";
application
	.filter('datetime', function ($filter) {
		return function (value, params) {
			if(!value)
				return '';
			if(!params) {
				if(value.length <= 10)
					params = 'DD/MM/YYYY';
				else
					params = 'DD/MM/YYYY HH:mm:ss';
			}
			moment.locale('pt-BR');
			return moment(value, "YYYY-MM-DD H:m:s").format(params);
		};
	})
	.filter('money', function () {
		return function (value) {
			var n = value, 
		    c = 2,
		    d = ',',
		    t = '.',
		    s = n < 0 ? "-" : "", 
		    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
		    j = (j = i.length) > 3 ? j % 3 : 0;
		   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
		};
	})
	;