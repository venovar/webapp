"use strict";
var application = angular
    .module('Application', [
        'ngMessages', 
        'ngSanitize', 
        'ngRoute',
        'ui.router',
        'ui.mask',
        'ngAnimate'
    ]);