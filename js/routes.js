"use strict";
application
    .config(function($stateProvider, $urlRouterProvider, $locationProvider) 
    {
        $urlRouterProvider.otherwise('/step/01');
        $stateProvider
            .state('404', {
                views : {
                    'content@' : {
                        templateUrl     : 'views/404.html?v=' + Math.random(),
                    }
                },
                url     : '/404',
            })
            /// Steps
            .state('step01', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/step/01.html?v=' + Math.random(),
                        controller      : 'Step01Controller',
                    },
                },
                url : '/step/01',
            })
            .state('step02', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/step/02.html?v=' + Math.random(),
                        controller      : 'Step02Controller',
                    },
                },
                url : '/step/02',
            })
            .state('step03', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/step/03.html?v=' + Math.random(),
                        controller      : 'Step03Controller',
                    },
                },
                url : '/step/03',
                params : {
                    responseData : null
                }
            })
            .state('unsuccess', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/step/unSuccess.html?v=' + Math.random(),
                        controller      : 'UnSuccessController',
                    },
                },
                url : '/step/unsuccess',
            })
        ;
    });