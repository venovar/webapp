"use strict";
application
	.config(function(
		$httpProvider, 
		$locationProvider
	) {
		$locationProvider.html5Mode(true);
		$httpProvider.interceptors.push('httpInterceptor');
	})
	.value('config', {
	    service_url : "https://api.noverde-hmg.net",
	});