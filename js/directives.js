"use strict";
application
	.directive('moneyFormat', function($q) {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModelController) {
				ngModelController.$asyncValidators.amount = function(modelValue, viewValue) {
					var deferred = $q.defer();
					if(modelValue && modelValue.toString().trim().length > 0)
						deferred.resolve();
					else
						deferred.reject();
					return deferred.promise;
				}
				/// IN
				ngModelController.$formatters.push(function(data) {
					if(data) {
						var n = data, 
					    c = 2,
					    d = ',',
					    t = '.',
					    s = n < 0 ? "-" : "", 
					    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
					    j = (j = i.length) > 3 ? j % 3 : 0;
					   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
					}
					return data;
				});
				/// MASK
				angular.element(element).mask('000.000.000.000.000,00', {reverse: true});
		        /// OUT
				ngModelController.$parsers.push(function(data) {
	    			if(data) {
	    				data = data.replace('R$', '');
	    				data = data.replace(' ', '');
	    				data = data.replace('.', '');
	    				data = data.replace(',', '.');
	    			}
	    			return parseFloat(data);
	  			});
			}
		}
	})
	.directive('cpfFormat', function($q) {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModelController) {
				// Convert model to view
				ngModelController.$formatters.push(function(data) {
					if(data) {
						data = data.replace(/\D/g, '');
						return data.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4');
					}
					return data;
				});

				// Mask
				angular.element(element).mask("000.000.000-00");
				
				ngModelController.$parsers.push(function(data) {
	    			if(data)
	      				data = data.replace(/\D/g, '');
	    			return data;
	  			});
	  			
				ngModelController.$asyncValidators.cpf = function(modelValue, viewValue) {
					var deferred = $q.defer();
					if(cpf(modelValue)) {
						deferred.resolve();
					} else {
						deferred.reject();
					}
					return deferred.promise;
				}
			}
		}
	})
;