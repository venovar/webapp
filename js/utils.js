function money (value, coin) {
    if(!value || isNaN(value))
    	return value;
    if(coin) {
	    if(coin == '$') {
	    	var 
			n = value, 
		    c = 2,
		    d = '.',
		    t = ',',
		    s = n < 0 ? "-" : "", 
		    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
		    j = (j = i.length) > 3 ? j % 3 : 0;
		   	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(-c) : "");
	    } else if(coin == 'BTC') {
	    	var 
			n = value, 
		    c = 8,
		    d = '.',
		    t = ',',
		    s = n < 0 ? "-" : "", 
		    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
		    j = (j = i.length) > 3 ? j % 3 : 0;
		   	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(-c) : "");
	    } 
    }
    var 
	n = value, 
    c = 2,
    d = ',',
    t = '.',
    s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
    j = (j = i.length) > 3 ? j % 3 : 0;
   	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(-c) : "");
}

function cpf (value) {
	if(!value)
		return false;
	$return = true;
   	var invalidos = [
        '11111111111',
        '22222222222',
        '33333333333',
        '44444444444',
        '55555555555',
        '66666666666',
        '77777777777',
        '88888888888',
        '99999999999',
        '00000000000'
    ];
    
    value = value.replace("-","");
    value = value.replace(/\./g,"");
    if(invalidos.indexOf(value) >= 0)
    	return false;

    //validando primeiro digito
    add = 0;
    for ( i=0; i < 9; i++ ) {
        add += parseInt(value.charAt(i), 10) * (10-i);
    }
    rev = 11 - ( add % 11 );
    if( rev == 10 || rev == 11) {
        rev = 0;
    }
    if( rev != parseInt(value.charAt(9), 10) ) {
        $return = false;
    }

    //validando segundo digito
    add = 0;
    for ( i=0; i < 10; i++ ) {
        add += parseInt(value.charAt(i), 10) * (11-i);
    }
    rev = 11 - ( add % 11 );
    if( rev == 10 || rev == 11) {
        rev = 0;
    }
    if( rev != parseInt(value.charAt(10), 10) ) {
        $return = false;
    }

    return $return;
}