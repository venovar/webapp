"use strict";
application
	.service('httpInterceptor', ['$rootScope', function($rootScope) {
		return {
			request: function(config) {
				$rootScope.$broadcast('request');
				return config;
			},
			requestError: function(rejection) {
				$rootScope.$broadcast('error', rejection.data);
				return rejection;
			},
			responseError: function(rejection) {
				$rootScope.$broadcast('error', rejection.data);
				return rejection;
			},
			response: function(response) {
				$rootScope.$broadcast('response');
				return response;
			},
		};
	}]);